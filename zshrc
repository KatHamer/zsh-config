#    _______| |__  _ __ ___ 
#   |_  / __| '_ \| '__/ __|
#  _ / /\__ \ | | | | | (__ 
# (_)___|___/_| |_|_|  \___|
#
# Kat Hamer's ZSH configuration

# Initialise completion
#source "$HOME/.zsh/completions.zsh"

# Configure history
source "$HOME/.zsh/history.zsh"

# Set environment variables
export EDITOR="nvim"
export PATH=$PATH:$HOME/.bin:$HOME/.local/bin

# Miscellaneous options
export ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=5'
setopt correct_all  # Correct typos
setopt auto_cd  # CD into directories without typing 'cd'

# Initialise Antibody plugin manager
source "$HOME/.zsh_plugins.sh"

# Some helpful functions + aliases

alias diff="colordiff"
alias ls='ls --color=auto'
alias ll='ls -l'
alias la='ls -A'
alias l='ls -CF'

# Custom prompt
autoload -U colors && colors  # Enable colours
autoload -U promptinit; promptinit
promptinit

# Bind keys for history search
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

# Bind key for go to start of line / end of line in VSCode
bindkey -e

# Set TERM to xterm-256color on SSH sessions to avoid missing terminfo issues
alias ssh='TERM=xterm-256color ssh'

# Mac specific
test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh" || true
alias code="/Applications/Visual\ Studio\ Code.app/Contents/Resources/app/bin/code"

# Poetry
fpath+=~/.zfunc
autoload -Uz compinit && compinit