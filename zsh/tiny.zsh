#!/bin/zsh
# tiny.zsh
# a tiny zsh prompt, no bloat included
# by Katelyn Hamer

setopt PROMPT_SUBST  # Allow substitution of variables in the prompt
setopt PROMPT_PERCENT  # Allow us to use percentage notation in the prompt

# Returns # or $ depending on whether we have root or not respectively
uidchar() {
	if (( $EUID == 0 )); then
		echo "#"
	else
		echo "$"
	fi
}

# Our actual prompt
# This line is messy just due to the way prompts are set. It works like this:
# First we set the text color to red with %{$fg[red]%}, then we echo the current user with $n and reset the colour
# Then we set the text color to blue with %{$fg[blue]%}, then we echo the current directory, shorthand if possible
# We then reset the text color and call the uidchar function which returns # if we are root, $ otherwise.
export PS1="%{$fg[red]%}%n%{$reset_color%} in %{$fg[blue]%}%~%{$reset_color%} $(uidchar) "  # E.g. kat in ~ $
