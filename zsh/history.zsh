# Kat's ZSH config - History
# Configure options for saving history

HISTFILE=$HOME/.zsh_history  # File to store history in
HISTSIZE=100000  # Maximum entries in history file
SAVEHIST=$HISTSIZE

setopt hist_ignore_all_dups  # Remove older duplicate entries from history
setopt hist_reduce_blanks  # Remove superfluous blanks from history items
setopt inc_append_history  # Save history entries as soon as they are entered
setopt share_history  # Share history between different instances of the shell

