#!/bin/bash
# Reinitialises ZSH config and installs Antibody + Plugins

# Paths
rc_file="./zshrc"
hist_file="./zsh_history"
zsh_directory="./zsh"
plugin_list_file="zsh_plugins.txt"

# Config
install_plugins="y"  # Install Antibody plugins to ~/.zsh_plugins.sh?
copy_hist_file="y"  # Copy existing zsh_history to ~/.zsh_history?
copy_zsh_directory="y"  # Copy zsh/ to ~/.zsh?
set_shell="y"  # Set default shell to zsh?

# If running from sudo, run a command as the regular user that invoked it
depriv() {
  if [[ $SUDO_USER ]]; then
    sudo -u "$SUDO_USER" -- "$@"
  else
    "$@"
  fi
}

# Echo an status message of type `$2` `$1`
put_status() {
    local stat_type=$2

    # Status types are 'error', 'ok' and 'warning', no arg is plain text
    case $stat_type in
        error)
            echo -e "\e[0;31mError: $1\e[0m"
            ;;
        ok)
            echo -e "\e[0;32m$1\e[0m"
            ;;
        warning)
            echo -e "\e[0;33mWarning: $1\e[0m"
            ;;
        *)
            echo "$1"
            ;;
        esac

}

# Copy source file `$1` to destination `$2` backing up existing files
# If `$3` is set the file is set to be owned by the user it references
copy_file() {
    local src="$1"
    local dest="$2"
    local owner="$3"

    if [[ -f "$dest" ]]; then
        # File exists, back it up
        mv "$dest" "$dest.old"
        echo "Backed up: $dest -> $dest.old"
    fi

    if cp "$src" "$dest"; then
        echo "Copied: $src -> $dest"
        if [[ -n "$owner" ]]; then
            if ! chown "$owner:$owner" "$dest"; then
                put_status "Couldn't set owner of $dest to $owner!"
            fi
        fi
    else
        put_status "Failed to copy $src to $dest!" error
        exit 1
    fi
}

init_zsh_config() {
    # If the script is invoked with sudo, we get the home folder and username of the user who ran sudo so we can install files to the correct locations and set permissions correctly
    # This is the only solution I could find that worked well
    # If we're not running from sudo we just use $USER and $HOME which are probably both root
    if [[ -n $SUDO_USER ]]; then
        user_home="$(getent passwd $SUDO_USER | cut -d: -f6)"
        user_name="$(getent passwd $SUDO_USER | cut -d: -f1)"
    else
        put_status "User is $USER!" warning
        user_home="$HOME"
        user_name=$USER
    fi

    # Check for git (needed for antibody)
    if ! which git >/dev/null 2>&1; then
        # Git not installed
        put_status "Git is required to run this script, please install it and rerun." error
        exit 1
    fi

    # Check for ZSH
    if which zsh >/dev/null 2>&1; then
        # ZSH is installed
        local has_zsh=1
    else
        put_status "To use this ZSH configuration you will need to install ZSH." warning
        local has_zsh=0
    fi

    # Check for existing ~/.zshrc
    if [[ -f "$user_home/.zshrc" ]]; then
        put_status "Detected an existing zshrc at $user_home/.zshrc!" warning

        local confirm_install
        read -p "Your existing configuration will be backed up and then overwritten. Are you sure you want to install this configuration? " confirm_install
        if ! [[ "$confirm_install" =~ y|Y|Yes|YES ]]; then
            put_status "Aborting!" error
            exit 2
        fi
    fi

    put_status "\nInstalling Antibody..." ok
    if ! curl -sfL git.io/antibody | sh -s - -b /usr/local/bin; then
        put_status "Failed to install Antibody. Aborting!" error
        exit 1
    fi

    put_status "\nCopying config files..." ok
    copy_file "$rc_file" "$user_home/.zshrc" "$user_name" || exit 1
    
    if [[ "$install_plugins" ]]; then
        copy_file "$plugin_list_file" "$user_home/.zsh_plugins.txt" "$user_name" || exit 1
    fi

    if [[ "$copy_hist_file" = "y" ]]; then
        copy_file "$hist_file" "$user_home/.zsh_history" "$user_name" || exit 1
    fi

    if [[ "$install_plugins" ]]; then
        put_status "\nInstalling Antibody plugins..." ok
        if ! depriv antibody bundle < "$user_home/.zsh_plugins.txt" > "$user_home/.zsh_plugins.sh"; then
            put_status "Failed to install Antibody plugins!" error
            exit 1
        else
            chown "$user_name:$user_name" "$user_home/.zsh_plugins.sh"
        fi

        if ! grep '.zsh_plugins.sh' "$user_home/.zshrc" >/dev/null 2>&1; then
            cat >> "$user_home/.zshrc" << EOT
                # Load Antibody plugins
                source ~/.zsh_plugins.sh
EOT
        fi
    fi

    if [[ "$copy_zsh_directory" = "y" ]]; then
        put_status "\nCopying zsh directory..." ok
        if [[ -f "$user_home/.zsh" ]]; then # Check for a file named ~/.zsh so we don't clobber it
            put_status "A file exists named $user_home/.zsh, not copying!" warning
        else
            if ! [[ -d "$user_home/.zsh" ]]; then
                cp -r "$zsh_directory" "$user_home/.zsh"
                chown -R "$user_name:$user_name" "$user_home/.zsh"
            else
                put_status "$user_home/.zsh already exists, not copying!" warning
            fi
        fi
    fi

    
    if [[ "$set_shell" = "y" ]]; then
        if [[ "$has_zsh" -eq 1 ]]; then
            put_status "\nSetting shell for $user_name to $(which zsh)..." ok
            chsh "$user_name" --shell "$(which zsh)"
        else
            put_status "You have the set_shell configuration option set to yes but you don't yet have ZSH installed.\nTo set ZSH as your default shell once it's installed you can run the 'chsh' binary." warning
        fi
    fi

    put_status "\n✓ All done!" ok
}

main() {
    # Check for root
    if [[ $EUID -ne 0 ]];
    then
            echo -e "\e[0;31mPlease run this script as root!\e[0m"
            exit 1
    fi  

    local confirm
    put_status "This script will install the ZSH configuration in this directory and optionally install Antibody and its plugins." ok
    read -p "  Do you want to continue? " confirm
    if [[ "$confirm" =~ y|Y|Yes|YES ]]; then
        echo
        init_zsh_config "$@"
    else
        put_status "Aborting!" warning
    fi
}

main "$@"
