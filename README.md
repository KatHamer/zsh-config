# My ZSH Config

# Files
zshrc - ZSH Config  
zsh_history - Optional ZSH history file  
zsh - Copied to ~/.zsh - any prompts or scripts can be placed here and referenced in configurations  
zsh_plugins.txt - Antibody plugin list  

# Installation

1. Install ZSH using your favourite package manager or compile from source
2. Clone this repo
3. Optionally, configure installation behaviour using the config values at the top of install.sh
4. Run install.sh inside the cloned repo as root, you will be presented with a semi-interactive installer script
